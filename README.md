# SG Markets Notebooks

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/sgmarkets%2Fsgmarkets-api-notebooks/master?filepath=notebooks)

This repo contains a collection of notebooks to help tap SG Market APIs.  
These [Jupyter](http://jupyter.org/) notebooks are meant to be used as inspiration.  

To run them
+ Locally: `git clone` the repo, install the packages in [requirements.txt](requirements.txt) and run `jupyter notebook`.
+ In the cloud: Click on the **Binder** badge above, and go inside the `notebooks/` folder, click on a notebook.  

To browse them
+ [demo-ROTB-vol.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/sgmarkets/sgmarkets-api-notebooks/raw/master/notebooks/demo-ROTB-vol.ipynb)
+ [ROTB-endpoint-strategy-components.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/sgmarkets/sgmarkets-api-notebooks/raw/master/notebooks/ROTB-endpoint-strategy-components.ipynb)
+ [ROTB-endpoint-strategy-prices.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/sgmarkets/sgmarkets-api-notebooks/raw/master/notebooks/ROTB-endpoint-strategy-prices.ipynb)
+ [ROTB-endpoint-strategy.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/sgmarkets/sgmarkets-api-notebooks/raw/master/notebooks/ROTB-endpoint-strategy.ipynb)
+ [demo-MarketData.ipynb](https://nbviewer.jupyter.org/urls/gitlab.com/sgmarkets/sgmarkets-api-notebooks/raw/master/notebooks/demo-MarketData.ipynb)

It is a work in progress. Notebooks are regularly updated and/or added.  

Here is the list of APIs covered:
+ [ROTB](https://analytics-api.sgmarkets.com/rotb/v1/swagger/ui/index) (Rates Option Trade Builder V1) through the Python package [sgmarkets-api-analytics-rotb](https://gitlab.com/sgmarkets/sgmarkets-api-analytics-rotb)
+ [MarketData](https://gitlab.com/sgmarkets/sgmarkets-api-analytics-market-data) (Analytics Data Api V2) through the Python package [sgmarkets-api-analytics-market-data](https://gitlab.com/sgmarkets/sgmarkets-api-market-data)


It also uses the following common modules
+ Authentication: [sgmarkets-api-auth](https://gitlab.com/sgmarkets/sgmarkets-api-auth)
+ Plot: [sgmarkets-plot](https://gitlab.com/sgmarkets/sgmarkets-plot)
